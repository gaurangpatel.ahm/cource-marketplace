package com.course.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.course.model.Cource;

@Repository
public interface CourseRepository extends JpaRepository<Cource, Long> {
	
	public List<Cource> findByInstituteId(Long instituteId);

	@Modifying(clearAutomatically = true)
	@Query("UPDATE Cource cource set cource.courseName =:courseName, cource.internationFees = :internationFees, cource.domesticFees = :domesticFees, cource.modifiedBy = :modifiedBy, cource.modifiedDate = :modifiedDate WHERE cource.id = :id")
	public void updateCource(@Param("id") Long courceId, @Param("courseName") String name, @Param("internationFees") int internationFees, @Param("domesticFees") int domesticFees,@Param("modifiedBy") UUID modifiedBy, @Param("modifiedDate") Long modifiedDate);
}
