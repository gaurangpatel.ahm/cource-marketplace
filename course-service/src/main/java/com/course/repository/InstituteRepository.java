package com.course.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.course.model.Institute;

@Repository
public interface InstituteRepository extends JpaRepository<Institute, Long> {

	public Institute findByName(String instituteName);
	
	@Modifying(clearAutomatically = true)
	@Query("UPDATE Institute inst set inst.name =:name, inst.country =:country, inst.modifiedBy = :modifiedBy, inst.modifiedDate = :modifiedDate where inst.id = :id")
	public void updateInstitute(@Param("id") Long id, @Param("name") String name, @Param("country") String country, @Param("modifiedBy") UUID modifiedBy, @Param("modifiedDate") Long modifiedDate);
	
}
