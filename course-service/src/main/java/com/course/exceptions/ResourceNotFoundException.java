package com.course.exceptions;

public class ResourceNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7041430257448765234L;

	public ResourceNotFoundException(String message) {
		super(message);
	}
}
