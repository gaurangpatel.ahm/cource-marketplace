package com.course.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.course.dto.KeycloakUser;

@FeignClient(name="user-service")	
public interface UserProxyService {

	   @RequestMapping(method = RequestMethod.GET, value = "/user-service/userinfo")
	   public ResponseEntity<KeycloakUser> getUserInfo();
}
