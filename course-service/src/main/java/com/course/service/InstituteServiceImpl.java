package com.course.service;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import com.course.dto.CourceDTO;
import com.course.dto.InstituteDTO;
import com.course.dto.KeycloakUser;
import com.course.exceptions.ResourceNotFoundException;
import com.course.model.Cource;
import com.course.model.Institute;
import com.course.repository.InstituteRepository;

@Service
public class InstituteServiceImpl implements InstituteService {
	Logger LOGGER = LoggerFactory.getLogger(getClass());
	private final InstituteRepository instituteRepository;
	private final ModelMapper modelMapper;
	private final UserProxyService userProxy;
	
	@Autowired
	public InstituteServiceImpl(InstituteRepository instituteRepository,
			ModelMapper modelMapper,
			UserProxyService userProxy) {
		this.instituteRepository=instituteRepository;
		this.modelMapper=modelMapper;
		this.userProxy=userProxy;
	}
	
	@Override
	public InstituteDTO createInstitute(InstituteDTO instituteDTO) {
		Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UUID userId = UUID.fromString(jwt.getSubject());
		Institute instituteEntity = convertToEntity(instituteDTO);
		instituteEntity.setCreatedDate(new Date().getTime());
		instituteEntity.setModifiedDate(new Date().getTime());
		instituteEntity.setCreatedBy(userId);
		instituteEntity.setModifiedBy(userId);
		return convertToDto(instituteRepository.save(instituteEntity));
	}
	
	@Override
	public InstituteDTO updateInstitute(InstituteDTO instituteDTO, Long instituteId) {
		Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UUID userId = UUID.fromString(jwt.getSubject());
		instituteRepository.updateInstitute(instituteId, instituteDTO.getName(), instituteDTO.getCountry(), userId, new Date().getTime());
		Institute instituteEntity = instituteRepository.findById(instituteId).orElseGet(null);
		if(instituteEntity!=null) {
			return convertToDto(instituteEntity);
		}
		return null;
	}

	@Override
	public InstituteDTO getInstituteByName(String instituteName) throws ResourceNotFoundException {
		Institute institute = instituteRepository.findByName(instituteName);
		if(institute == null) {
			throw new ResourceNotFoundException(String.format("Requested resource with resource name %s not found",instituteName));
		}
		return convertToDto(institute);
	}

	@Override
	public InstituteDTO getInstituteById(Long instituteId) throws ResourceNotFoundException {
		KeycloakUser user = userProxy.getUserInfo().getBody();
		String userCountry=user.getCountryCode();
		LOGGER.debug("Login user info: {}",user);
		
		Institute institute=instituteRepository.findById(instituteId).orElse(null);
		if(institute==null) {
			throw new ResourceNotFoundException(String.format("Requested Institute with id %s not found",instituteId));
		}
		InstituteDTO instituteDTO = convertToDto(institute);
		boolean domestic = userCountry.equals(institute.getCountry());
		if(instituteDTO.getCources()!=null) {
			instituteDTO.getCources().forEach(cource -> {
				cource.setEffectivePrice(domestic ? cource.getDomesticFees() : cource.getInternationFees());
			});
		}
		return instituteDTO;
	}
	
	private Institute convertToEntity(InstituteDTO instituteDTO) {
		return modelMapper.map(instituteDTO, Institute.class);
	}

	private InstituteDTO convertToDto(Institute institute) {
		InstituteDTO result = new InstituteDTO();
		result.setId(institute.getId());
		result.setName(institute.getName());
		result.setCountry(institute.getCountry());
		if(institute.getCources()!=null) {
			result.setCources(institute.getCources().stream().map(c -> convertToDto(c)).collect(Collectors.toSet()));			
		}
		return result;
	}
	
	private CourceDTO convertToDto(Cource cource) {
		return modelMapper.map(cource, CourceDTO.class);
	}
}
