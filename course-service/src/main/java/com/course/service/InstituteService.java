package com.course.service;

import com.course.dto.InstituteDTO;
import com.course.exceptions.ResourceNotFoundException;

public interface InstituteService {

	public InstituteDTO createInstitute(InstituteDTO institute);
	
	public InstituteDTO updateInstitute(InstituteDTO instituteDTO, Long instituteId);
	
	public InstituteDTO getInstituteByName(String instituteName) throws ResourceNotFoundException;
	
	public InstituteDTO getInstituteById(Long instituteId) throws ResourceNotFoundException;

}
