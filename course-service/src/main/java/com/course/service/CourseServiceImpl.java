package com.course.service;

import java.util.Date;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import com.course.dto.CourceDTO;
import com.course.model.Cource;
import com.course.repository.CourseRepository;
import com.course.repository.InstituteRepository;

@Service
public class CourseServiceImpl implements CourseService {

	private final CourseRepository courseRepository;
	private final InstituteRepository instituteRepository;
	private final ModelMapper modelMapper;
	
	@Autowired
	public CourseServiceImpl(CourseRepository courseRepository,
			ModelMapper modelMapper,
			InstituteRepository instituteRepository
	) {
		this.courseRepository=courseRepository;
		this.modelMapper=modelMapper;
		this.instituteRepository = instituteRepository;
	}
	
	@Override
	public CourceDTO createCourse(CourceDTO courcesDTO, Long instituteId) {
		Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UUID userId = UUID.fromString(jwt.getSubject());
		
		Cource courceEntity = convertToEntity(courcesDTO);
		courceEntity.setCreatedDate(new Date().getTime());
		courceEntity.setModifiedDate(new Date().getTime());
		courceEntity.setCreatedBY(userId);
		courceEntity.setModifiedBy(userId);
		courceEntity.setInstitute(instituteRepository.findById(instituteId).orElse(null));
		return convertToDto(courseRepository.save(courceEntity));
	}

	@Override
	public CourceDTO updateCource(CourceDTO cource, Long instituteId, Long courceId) {
		Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		UUID userId = UUID.fromString(jwt.getSubject());
		courseRepository.updateCource(courceId, cource.getCourseName(), cource.getInternationFees(), cource.getDomesticFees(), userId, new Date().getTime());
		Cource courceEntity = courseRepository.findById(courceId).orElseGet(null);
		if(courceEntity!=null) {
			return convertToDto(courceEntity);
		}
		return null;
	}

	private Cource convertToEntity(CourceDTO courceDTO) {
		return modelMapper.map(courceDTO, Cource.class);
	}

	private CourceDTO convertToDto(Cource cource) {
		return modelMapper.map(cource, CourceDTO.class);
	}
}
