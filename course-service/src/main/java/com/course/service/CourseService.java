package com.course.service;

import com.course.dto.CourceDTO;

public interface CourseService {

	public CourceDTO createCourse(CourceDTO cources, Long instituteId);
	public CourceDTO updateCource(CourceDTO cource, Long instituteId, Long courceId);
}
