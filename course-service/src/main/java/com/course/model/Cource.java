package com.course.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = {
	      @UniqueConstraint(columnNames = "courseName", name = "uniqueNameConstraint")}
	, name = "cource")
public class Cource {
	@Id
	@GeneratedValue
	private Long id;
	private String courseName;
	private int internationFees;
	private int domesticFees;
	private long createdDate;
	private UUID createdBY;
	private long modifiedDate;
	private UUID modifiedBy;
	@ManyToOne
	@JoinColumn(name="institute_id", nullable=false)
	private Institute institute;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getInternationFees() {
		return internationFees;
	}

	public void setInternationFees(int internationFees) {
		this.internationFees = internationFees;
	}

	public int getDomesticFees() {
		return domesticFees;
	}

	public void setDomesticFees(int domesticFees) {
		this.domesticFees = domesticFees;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public UUID getCreatedBY() {
		return createdBY;
	}

	public void setCreatedBY(UUID createdBY) {
		this.createdBY = createdBY;
	}

	public long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public UUID getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(UUID modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	@Override
	public String toString() {
		return "Cource [id=" + id + ", courseName=" + courseName + ", internationFees=" + internationFees
				+ ", domesticFees=" + domesticFees + ", createdDate=" + createdDate + ", createdBY=" + createdBY
				+ ", modifiedDate=" + modifiedDate + ", modifiedBy=" + modifiedBy + ", institute=" + institute + "]";
	}

}
