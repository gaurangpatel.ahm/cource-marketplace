package com.course.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@Component
public class FeignClientInterceptor implements RequestInterceptor {
	Logger LOGGER = LoggerFactory.getLogger(getClass());
	private static final String AUTHORIZATION_HEADER="Authorization";
	private static final String TOKEN_TYPE = "Bearer";
	
	@Override
	public void apply(RequestTemplate requestTemplate) {
		try {
			Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			requestTemplate.header(AUTHORIZATION_HEADER, String.format("%s %s", TOKEN_TYPE, jwt.getTokenValue().toString()));
		} catch (Exception e) {
			LOGGER.error("Exception while applying Authorization header.",e);
	    }
	}
}