package com.course.dto;

import java.util.List;
import java.util.UUID;

public class KeycloakUser {
	private UUID id;
	private UUID sub;
	private String name;
	private String registeredrole;
	private String preferred_username;
	private String given_name;
	private String family_name;
	private String email;
	private String countryCode;
	private List<String> effectiveRoles;

	public List<String> getEffectiveRoles() {
		return effectiveRoles;
	}

	public void setEffectiveRoles(List<String> effectiveRoles) {
		this.effectiveRoles = effectiveRoles;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getSub() {
		return sub;
	}

	public void setSub(UUID sub) {
		this.sub = sub;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPreferred_username() {
		return preferred_username;
	}

	public void setPreferred_username(String preferred_username) {
		this.preferred_username = preferred_username;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getFamily_name() {
		return family_name;
	}

	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegisteredrole() {
		return registeredrole;
	}

	public void setRegisteredrole(String registeredrole) {
		this.registeredrole = registeredrole;
	}

	@Override
	public String toString() {
		return "KeycloakUser [id=" + id + ", sub=" + sub + ", name=" + name + ", registeredrole=" + registeredrole
				+ ", preferred_username=" + preferred_username + ", given_name=" + given_name + ", family_name="
				+ family_name + ", email=" + email + ", countryCode=" + countryCode + ", effectiveRoles="
				+ effectiveRoles + "]";
	}

}
