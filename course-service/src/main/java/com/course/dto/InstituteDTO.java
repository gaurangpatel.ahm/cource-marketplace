package com.course.dto;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotEmpty;

public class InstituteDTO {
	private Long id;
	@NotEmpty(message = "Institute name is required")
	private String name;
	@NotEmpty(message = "Country is requried")
	private String country;
	private Set<CourceDTO> cources;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Set<CourceDTO> getCources() {
		return cources;
	}
	public void setCources(Set<CourceDTO> cources) {
		this.cources = cources;
	}
	@Override
	public String toString() {
		return "InstituteDTO [id=" + id + ", name=" + name + ", country=" + country + ", cources=" + cources + "]";
	}
}
