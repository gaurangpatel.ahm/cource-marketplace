package com.course.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class CourceDTO {
	private Long id;
	@NotEmpty(message = "courseName is required")
	private String courseName;
	@Min(value = 1, message = "Value for International Fees is required")
	private int internationFees;
	@Min(value = 1, message = "Value for Domestic Fees is required")
	private int domesticFees;
	private int effectivePrice;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public int getInternationFees() {
		return internationFees;
	}
	public void setInternationFees(int internationFees) {
		this.internationFees = internationFees;
	}
	public int getDomesticFees() {
		return domesticFees;
	}
	public void setDomesticFees(int domesticFees) {
		this.domesticFees = domesticFees;
	}
	public int getEffectivePrice() {
		return effectivePrice;
	}
	public void setEffectivePrice(int effectivePrice) {
		this.effectivePrice = effectivePrice;
	}
	@Override
	public String toString() {
		return "CourceDTO [id=" + id + ", courseName=" + courseName + ", internationFees=" + internationFees
				+ ", domesticFees=" + domesticFees + ", effectivePrice=" + effectivePrice + "]";
	}	
}
