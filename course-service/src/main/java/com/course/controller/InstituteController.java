package com.course.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.course.dto.CourceDTO;
import com.course.dto.InstituteDTO;
import com.course.exceptions.ResourceNotFoundException;
import com.course.service.CourseService;
import com.course.service.InstituteService;

@RestController
public class InstituteController {

	private final InstituteService instituteService;
	private final CourseService courceService;

	@Autowired
	public InstituteController(InstituteService instituteService, 
			CourseService courceService) {
		this.instituteService = instituteService;
		this.courceService = courceService;
	}
	
	@RequestMapping(method = RequestMethod.POST,path =  "/institute")
	public ResponseEntity<InstituteDTO> createInstitute(@RequestBody @Valid InstituteDTO instituteDTO) {
		InstituteDTO created = instituteService.createInstitute(instituteDTO);
		URI location = URI.create(String.format("/institute/%s", created.getId()));
		return ResponseEntity.created(location).body(created);
	}
	
	@RequestMapping(method = RequestMethod.PUT,path =  "/institute/{instituteId}")
	public ResponseEntity<InstituteDTO> updateInstitute(@RequestBody @Valid InstituteDTO instituteDTO, @PathVariable("instituteId") Long instituteId) {
		InstituteDTO updated = instituteService.updateInstitute(instituteDTO,instituteId);
		return ResponseEntity.ok().body(updated);
	}

	
	@RequestMapping(method = RequestMethod.POST,path =  "/institute/{instituteId}/cource")
	public ResponseEntity<CourceDTO> createCource(@RequestBody @Valid CourceDTO cource, @PathVariable("instituteId") Long instituteId) {
		CourceDTO created = courceService.createCourse(cource,instituteId);
		URI location = URI.create(String.format("/institute/%s/cource/%s", instituteId,created.getId()));
		return ResponseEntity.created(location).body(created);
	}
	
	@RequestMapping(method = RequestMethod.PUT,path =  "/institute/{instituteId}/cource/{courceId}")
	public ResponseEntity<CourceDTO> updateCource(@RequestBody @Valid CourceDTO cource, @PathVariable("instituteId") Long instituteId, @PathVariable("courceId") Long courceId) {
		CourceDTO updated = courceService.updateCource(cource, instituteId, courceId);
		return ResponseEntity.ok().body(updated);
	}
	
	@RequestMapping(method = RequestMethod.GET,path =  "/institute/{instituteId}")
	public ResponseEntity<InstituteDTO> findInstituteById(@PathVariable("instituteId") Long instituteId) throws ResourceNotFoundException {
		InstituteDTO institute = instituteService.getInstituteById(instituteId);
		return ResponseEntity.ok().body(institute);
	}	
}
