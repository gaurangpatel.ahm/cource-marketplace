package com.cloud.gateway;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.oidc.web.server.logout.OidcClientInitiatedServerLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;

import reactor.core.publisher.Mono;

@Configuration
public class SecurityConfig {

	@Bean
	public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http,
			ReactiveClientRegistrationRepository clientRegistrationRepository) {
		http.logout(logout -> logout.logoutSuccessHandler(new OidcClientInitiatedServerLogoutSuccessHandler(
				clientRegistrationRepository)));
		http.authorizeExchange()
		.pathMatchers(HttpMethod.POST, "/course-service/**").hasRole("ADMIN")
		.pathMatchers(HttpMethod.PUT, "/course-service/**").hasRole("ADMIN")
		.pathMatchers(HttpMethod.GET, "/course-service/**").hasAnyRole("USER","ADMIN")
		.pathMatchers(HttpMethod.GET, "/user-service/**").hasAnyRole("USER","ADMIN")
		.pathMatchers(HttpMethod.POST, "/user-service/**").permitAll()
		.pathMatchers(HttpMethod.PUT, "/user-service/**").permitAll()
		.and()
		.oauth2ResourceServer().jwt()
		.jwtAuthenticationConverter(grantedAuthoritiesExtractor());
		
		http.csrf().disable();
		http.cors();
		http.headers().frameOptions().disable();
		return http.build();
	}
	
	Converter<Jwt, Mono<AbstractAuthenticationToken>> grantedAuthoritiesExtractor() {
	    GrantedAuthoritiesExtractor extractor = new GrantedAuthoritiesExtractor();
	    return new ReactiveJwtAuthenticationConverterAdapter(extractor);
	}
}

class GrantedAuthoritiesExtractor extends JwtAuthenticationConverter {
    protected Collection<GrantedAuthority> extractAuthorities(Jwt jwt) {
    	
    	final Map<String, Object> realmAccess = (Map<String, Object>) jwt.getClaims().get("realm_access");
		return ((List<String>) realmAccess.get("roles")).stream()
				.map(roleName -> "ROLE_" + roleName)
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
    }
}
 
 