package com.user.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "keycloak")
public class KeycloakConfig {
	private String serviceUrl;
	private String realm;
	private String clientId;
	private String clientSecret;
	private String targetRealm;
	private String targetClientId;
	
	public String getTargetClientId() {
		return targetClientId;
	}
	public void setTargetClientId(String targetClientId) {
		this.targetClientId = targetClientId;
	}
	public String getTargetRealm() {
		return targetRealm;
	}
	public void setTargetRealm(String targetRealm) {
		this.targetRealm = targetRealm;
	}
	public String getServiceUrl() {
		return serviceUrl;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	public String getRealm() {
		return realm;
	}
	public void setRealm(String realm) {
		this.realm = realm;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	@Override
	public String toString() {
		return "KeycloakConfig [serviceUrl=" + serviceUrl + ", realm=" + realm + ", clientId=" + clientId
				+ ", clientSecret=" + clientSecret + ", targetRealm=" + targetRealm + ", targetClientId="
				+ targetClientId + "]";
	}
}
