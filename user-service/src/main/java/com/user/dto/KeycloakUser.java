package com.user.dto;

import java.util.List;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class KeycloakUser {
	private UUID id;
	private UUID sub;
	@NotEmpty(message = "name is required")
	private String name;
	@NotEmpty(message = "registeredrole is required")
	private String registeredrole;
	private String preferred_username;
	@NotEmpty(message = "Given name is required")
	private String given_name;
	private String family_name;
	@NotEmpty(message = "Email is mandatory")
	@Email(message = "Please provide valid email")
	private String email;
	private String password;
	@NotEmpty(message = "Please provide country code")
	private String countryCode;
	private List<String> effectiveRoles;

	public List<String> getEffectiveRoles() {
		return effectiveRoles;
	}

	public void setEffectiveRoles(List<String> effectiveRoles) {
		this.effectiveRoles = effectiveRoles;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getSub() {
		return sub;
	}

	public void setSub(UUID sub) {
		this.sub = sub;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPreferred_username() {
		return preferred_username;
	}

	public void setPreferred_username(String preferred_username) {
		this.preferred_username = preferred_username;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getFamily_name() {
		return family_name;
	}

	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRegisteredrole() {
		return registeredrole;
	}

	public void setRegisteredrole(String registeredrole) {
		this.registeredrole = registeredrole;
	}

	@Override
	public String toString() {
		return "KeycloakUser [id=" + id + ", sub=" + sub + ", name=" + name + ", registeredrole=" + registeredrole
				+ ", preferred_username=" + preferred_username + ", given_name=" + given_name + ", family_name="
				+ family_name + ", email=" + email + ", password=" + password + ", countryCode=" + countryCode + "]";
	}

}
