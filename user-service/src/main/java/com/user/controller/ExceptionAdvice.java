package com.user.controller;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.user.dto.CustomErrorResponseDTO;
import com.user.exceptions.ResourceNotFoundException;


@RestControllerAdvice
public class ExceptionAdvice {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionAdvice.class);

	@ExceptionHandler(value = ResourceNotFoundException.class)
	public ResponseEntity<CustomErrorResponseDTO> handleGenericNotFoundException(ResourceNotFoundException e) {
		CustomErrorResponseDTO error = new CustomErrorResponseDTO("NOT_FOUND_ERROR", e.getMessage());
		error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_FOUND.value()));
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
		
	@ExceptionHandler(value = Throwable.class)
	public ResponseEntity<CustomErrorResponseDTO> handleGenericException(Throwable e) {
		CustomErrorResponseDTO error = new CustomErrorResponseDTO("INTERNAL_SERVER_ERROR", e.getMessage());
		error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.INTERNAL_SERVER_ERROR.value()));
        LOGGER.error("Unknown Exception occured ",e);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
