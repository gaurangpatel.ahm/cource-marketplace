package com.user.controller;

import java.net.URI;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user.dto.KeycloakUser;
import com.user.exceptions.ResourceNotFoundException;
import com.user.service.KeycloakCreateUserService;

@RestController
public class UserController {

	private final KeycloakCreateUserService keycloakCreateUserService;
	
	@Autowired
	public UserController(KeycloakCreateUserService keycloakCreateUserService) {
		this.keycloakCreateUserService = keycloakCreateUserService;
	}
	
	@PostMapping("/user")
	public ResponseEntity<KeycloakUser> createUser(@RequestBody @Valid KeycloakUser user) {
		KeycloakUser created = keycloakCreateUserService.createUser(user);
		if(created!=null) {
			URI location = URI.create(String.format("/user/%s", created.getId()));
			return ResponseEntity.created(location).body(created);			
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("/user/{userId}")
	public ResponseEntity<KeycloakUser> updateUser(@RequestBody @Valid KeycloakUser user, @PathVariable("userId") UUID userId) {
		user.setId(userId);
		KeycloakUser updated = keycloakCreateUserService.updateUser(user);
		return ResponseEntity.ok().body(updated);
	}
	
	@GetMapping("/user")
	public ResponseEntity<?> getUserByEmail(@RequestParam("email") String email) throws ResourceNotFoundException{
		KeycloakUser user = keycloakCreateUserService.getByEmail(email);
		return ResponseEntity.ok().body(user);			
	}
	
	@GetMapping("/userinfo")
	public ResponseEntity<KeycloakUser> userInfo() throws ResourceNotFoundException {
		return ResponseEntity.ok().body(keycloakCreateUserService.getUserInfo());
	}
}
