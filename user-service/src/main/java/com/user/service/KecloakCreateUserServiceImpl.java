package com.user.service;


import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.entity.ContentType;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import com.user.config.KeycloakConfig;
import com.user.dto.KeycloakUser;
import com.user.dto.TokenEntity;
import com.user.exceptions.ResourceNotFoundException;

@Service
public class KecloakCreateUserServiceImpl implements KeycloakCreateUserService {

	Keycloak kc;
	private final KeycloakConfig kcConfig;
	Logger logger = LoggerFactory.getLogger(getClass());
	ResteasyClient client = new ResteasyClientBuilder().build();
	KeycloackServiceInterface keyCloakProxy;
    RealmResource realmResource;
    UsersResource usersRessource;
	
	public KecloakCreateUserServiceImpl(@Autowired KeycloakConfig kcConfig) {
		this.kcConfig = kcConfig;
		
		ResteasyWebTarget target = client.target(UriBuilder.fromPath(kcConfig.getServiceUrl()+"/realms/"+kcConfig.getTargetRealm()));
		this.keyCloakProxy = target.proxy(KeycloackServiceInterface.class);
		logger.info("Service URL: {} \n"
				+ "Realm: {} \n"
				+ "clieantID: {} \n"
				+ "clientSecret: {} ",
				kcConfig.getServiceUrl(),
				kcConfig.getRealm(),
				kcConfig.getClientId(),
				kcConfig.getClientSecret());
		kc = KeycloakBuilder.builder() 
				.serverUrl(kcConfig.getServiceUrl()) 
				.realm(kcConfig.getRealm())
				.grantType(OAuth2Constants.PASSWORD) 
				.clientId(kcConfig.getClientId())
				.clientSecret(kcConfig.getClientSecret())
				.username("admin")
				.password("pass")
				.build();
		 realmResource = kc.realm(kcConfig.getTargetRealm());
		 usersRessource = realmResource.users();

//		System.out.println(kc.realm(kcConfig.getRealm()).users().count());
	}
	
	private UserRepresentation createUserRep(KeycloakUser user) {
		CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(user.getPassword());
        
		UserRepresentation userrep = new UserRepresentation();
		userrep.setEnabled(true);
		userrep.setEmailVerified(true);
		userrep.setUsername(user.getName());
		userrep.setFirstName(user.getGiven_name());
		userrep.setLastName(user.getFamily_name());
		userrep.setEmail(user.getEmail());
		userrep.setCredentials(Arrays.asList(passwordCred));
		userrep.setEnabled(true);
		userrep.singleAttribute("CountryCode", user.getCountryCode());
       return userrep;
	}
	
	@Override
	public KeycloakUser createUser(KeycloakUser user) {
		String userId="";
		KeycloakUser created=null;
		try {
		UserRepresentation userrep = createUserRep(user);
		
        Response response = usersRessource.create(userrep);
        logger.info("Repsonse: {}", response.getStatus());
        System.out.println(response.getStatusInfo());
        userId = CreatedResponseUtil.getCreatedId(response);
        
        
        System.out.printf("User created with userId: %s%n", userId);
		UserResource userResource = usersRessource.get(userId);
		
		RoleRepresentation userRealmRole = realmResource.roles()//
                .get(user.getRegisteredrole()).toRepresentation();
		
		userResource.roles().realmLevel() //
        	.add(Arrays.asList(userRealmRole));
		
		List<UserRepresentation> createdUser = usersRessource.search(user.getName());
		
//		logger.info("{}",userResource.roles().realmLevel().listEffective());
		logger.info("Created : "+createdUser.get(0));
		UserRepresentation createdUserRep = createdUser.get(0);
		created = createKeyloakUser(createdUserRep, userResource);
		}catch (Exception e) {
			System.out.println("User already exist");
		}
        return created;
	}
	
	private KeycloakUser createKeyloakUser(UserRepresentation userrep, UserResource userResource) {
		KeycloakUser user = new KeycloakUser();
		user.setSub(UUID.fromString(userrep.getId()));
		user.setCountryCode(userrep.getAttributes().get("CountryCode").get(0));
		user.setEmail(userrep.getEmail());
		user.setGiven_name(userrep.getFirstName());
		user.setFamily_name(userrep.getLastName());
		user.setId(UUID.fromString(userrep.getId()));
		user.setName(userrep.getUsername());
		user.setPreferred_username(userrep.getUsername());
		user.setRegisteredrole(userResource.roles().realmLevel().listEffective().get(0).getName());
		user.setEffectiveRoles(userResource.roles().realmLevel().listEffective().stream().map(r -> r.getName()).collect(Collectors.toList()));
		return user;
	}

	@Override
	public TokenEntity createUserToken(String username, String password) {
		Form form = new Form();
		form.param("grant_type", "password");
		form.param("username", username);
		form.param("password", password);
		form.param("client_id",kcConfig.getTargetClientId());
		TokenEntity response = keyCloakProxy.getToken(form, ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
		return response; 
	}

	@Override
	public KeycloakUser updateUser(KeycloakUser user) {
		String userId="";
		KeycloakUser updated=null;
		try {
		UserRepresentation userrep = createUserRep(user);
		userId = user.getId().toString();
        
        System.out.printf("User created with userId: %s%n", userId);
		UserResource userResource = usersRessource.get(userId);
		userResource.update(userrep);
		RoleRepresentation userRealmRole = realmResource.roles()//
                .get("USER").toRepresentation();
		
		userResource.roles().realmLevel() //
        	.add(Arrays.asList(userRealmRole));
		
		List<UserRepresentation> createdUser = usersRessource.search(user.getName());
		
		logger.info("{}",userResource.roles().realmLevel().listEffective());
		logger.info("Created : "+createdUser.get(0));
		UserRepresentation createdUserRep = createdUser.get(0);
		updated = createKeyloakUser(createdUserRep, userResource);
		}catch (Exception e) {
			System.out.println("User already exist");
		}
        return updated;
	}

	@Override
	public KeycloakUser getByEmail(String email) throws ResourceNotFoundException {
		List<UserRepresentation> foundUser = usersRessource.search(email);
		if(foundUser.size()==0) {
			throw new ResourceNotFoundException(String.format("User with email %s not found",email));
		}
		UserRepresentation createdUserRep = foundUser.get(0);
		UserResource userResource = usersRessource.get(createdUserRep.getId());
		KeycloakUser found = createKeyloakUser(createdUserRep, userResource);
		return found;
	}

	@Override
	public KeycloakUser getUserInfo() throws ResourceNotFoundException {
		Jwt jwt = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserResource userResource = usersRessource.get(jwt.getSubject());
		if(userResource==null) {
			throw new ResourceNotFoundException(String.format("User with subject id %s not found", jwt.getSubject()));
		}
		KeycloakUser found = createKeyloakUser(userResource.toRepresentation(), userResource);
		return found;
	}	
}
