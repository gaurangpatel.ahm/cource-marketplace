package com.user.service;

import com.user.dto.KeycloakUser;
import com.user.dto.TokenEntity;
import com.user.exceptions.ResourceNotFoundException;

public interface KeycloakCreateUserService {
	public KeycloakUser createUser(KeycloakUser user);
	public KeycloakUser updateUser(KeycloakUser user);
	public KeycloakUser getByEmail(String email) throws ResourceNotFoundException;
	public TokenEntity createUserToken(String username, String password);
	public KeycloakUser getUserInfo() throws ResourceNotFoundException;
}
