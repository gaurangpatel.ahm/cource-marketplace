package com.user.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import com.user.dto.TokenEntity;


@Path("/protocol/openid-connect")
public interface KeycloackServiceInterface {
	@POST
    @Path("/token")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_XML })
	TokenEntity getToken(Form request,
    		@HeaderParam("Content-Type") String contentType);
}
